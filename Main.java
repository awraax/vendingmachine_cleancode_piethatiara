package vendingmachine.step3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
         VendingMachineInterface machineInterface = new TextVendingMachineInterface();

         try{
                 machineInterface.displayProducts();
                 String selectedProduct = scanner.nextLine();
                 machineInterface.selectProduct(Integer.parseInt(selectedProduct));

                 machineInterface.displayEnterCoinsMessage();
                 String userEnteredCoins = scanner.nextLine();

                 try{
                         int[] enteredCoins = Coin.parseCoins(userEnteredCoins);
                         machineInterface.enterCoins(enteredCoins);

                         machineInterface.displayChangeMessage();
                 } catch (NumberFormatException | ArrayIndexOutOfBoundsException e){
                         System.out.println(e);
                 }

         } catch (InputMismatchException e){
                 System.out.println("Invalid Input");
         }
    }
}