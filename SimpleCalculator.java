package vendingmachine.step3;

public class SimpleCalculator implements Calculator {
    @Override
    public int calculateTotal(CoinBundle enteredCoins) {
        return enteredCoins.getTotal();
    }

    @Override
    public CoinBundle calculateChange(int amountMoneyToReturn) {
        CoinBundle change = new CoinBundle(new int[5]);
        int remainingAmount = amountMoneyToReturn;

        change.number100CentsCoins = div(remainingAmount, Coin.HUNDRED_CENTS.getValue());
        remainingAmount = amount(remainingAmount, Coin.HUNDRED_CENTS.getValue());

        change.number50CentsCoins = div(remainingAmount, Coin.FIFTY_CENTS.getValue());
        remainingAmount = amount(remainingAmount, Coin.FIFTY_CENTS.getValue());

        change.number20CentsCoins = div(remainingAmount, Coin.TWENTY_CENTS.getValue());
        remainingAmount = amount(remainingAmount, Coin.TWENTY_CENTS.getValue());

        change.number10CentsCoins = div(remainingAmount, Coin.TEN_CENTS.getValue());
        remainingAmount = amount(remainingAmount, Coin.TEN_CENTS.getValue());

        change.number5CentsCoins = div(remainingAmount, Coin.FIVE_CENTS.getValue());
        return change;
    }

    public int div(int num1, int num2){return num1/num2;}

    public int amount(int num1, int num2){return num1%num2;}

    public int coin(int coin, int coinBundle){return coin*coinBundle;}

    public int totalCoin(int total, int coin){return total+coin;}
}
